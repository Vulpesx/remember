use crate::{
    lexer::{Lexer, Loc, Token, TokenKind},
    loc_here, Reminder,
};

pub fn get_command<C: Iterator<Item = char>>(lexer: &mut Lexer<C>) -> Option<Command> {
    let token = lexer.next();
    match token {
        Some(token) => match token.kind {
            TokenKind::Quit => Some(Command::Quit),
            TokenKind::Remind => Some(Command::Remind),
            TokenKind::List => Some(Command::List),
            TokenKind::Edit => Some(Command::Edit),
            TokenKind::Help => Some(Command::Help),
            _ => Some(Command::Invalid(lexer.next())),
        },
        None => None,
    }
}

pub fn parse_duration<C: Iterator<Item = char>>(
    lexer: &mut Lexer<C>,
) -> Result<Reminder, ParserError> {
    match lexer.expect_token(TokenKind::Num) {
        Ok(token) => {
            let duration = token.text.parse::<u32>().unwrap();
            let summary = match lexer.expect_token(TokenKind::Str) {
                Ok(token) => token.text,
                Err(token) => match parser_error(token, TokenKind::Str) {
                    ParserError::NoToken(_) => "No sumary provided".into(),
                    p => return Err(p),
                },
            };
            let description = match lexer.next() {
                Some(token) => match token {
                    Token {
                        kind: TokenKind::Str,
                        text,
                        ..
                    } => Some(text),
                    token => match parser_error(token, TokenKind::Str) {
                        ParserError::NoToken(_) => None,
                        p => return Err(p),
                    },
                },
                None => None,
            };
            Ok(Reminder::new(
                crate::When::Duration(duration),
                summary,
                description,
            ))
        }
        Err(token) => Err(parser_error(token, TokenKind::Num)),
    }
}

pub fn parse_time<C: Iterator<Item = char>>(lexer: &mut Lexer<C>) -> Result<Reminder, ParserError> {
    match lexer.expect_token(TokenKind::Num) {
        Ok(token) => {
            let hour = token.text.parse::<u32>().unwrap();
            if hour > 23 {
                return Err(ParserError::InvalidNum(token.loc, hour as i32, 0, 23));
            }
            let minute = match lexer.expect_token(TokenKind::Num) {
                Ok(token) => token.text.parse::<u32>().unwrap(),
                Err(token) => return Err(parser_error(token, TokenKind::Num)),
            };
            if minute > 59 {
                return Err(ParserError::InvalidNum(token.loc, minute as i32, 0, 59));
            }
            let summary = match lexer.expect_token(TokenKind::Str) {
                Ok(token) => token.text,
                Err(token) => match parser_error(token, TokenKind::Str) {
                    ParserError::NoToken(_) => "No sumary provided".into(),
                    p => return Err(p),
                },
            };
            let description = match lexer.next() {
                Some(token) => match token {
                    Token {
                        kind: TokenKind::Str,
                        text,
                        ..
                    } => Some(text),
                    token => match parser_error(token, TokenKind::Str) {
                        ParserError::NoToken(_) => None,
                        p => return Err(p),
                    },
                },
                None => None,
            };
            Ok(Reminder::new(
                crate::When::Time(hour, minute),
                summary,
                description,
            ))
        }

        Err(token) => Err(parser_error(token, TokenKind::Num)),
    }
}

pub fn parse_day<C: Iterator<Item = char>>(lexer: &mut Lexer<C>) -> Result<Reminder, ParserError> {
    match lexer.expect_token(TokenKind::Str) {
        Ok(token) => {
            let day = token.text;
            if !"montuewedthufrisatsun".contains(&day) {
                return Err(ParserError::InvalidDay(token.loc, day));
            }
            let hour = match lexer.expect_token(TokenKind::Num) {
                Ok(token) => token.text.parse::<u32>().unwrap(),
                Err(token) => return Err(parser_error(token, TokenKind::Num)),
            };
            if hour > 23 {
                return Err(ParserError::InvalidNum(lexer.loc(), hour as i32, 0, 23));
            }
            let minute = match lexer.expect_token(TokenKind::Num) {
                Ok(token) => token.text.parse::<u32>().unwrap(),
                Err(token) => return Err(parser_error(token, TokenKind::Num)),
            };
            if minute > 59 {
                return Err(ParserError::InvalidNum(lexer.loc(), minute as i32, 0, 59));
            }
            let summary = match lexer.expect_token(TokenKind::Str) {
                Ok(token) => token.text,
                Err(token) => match parser_error(token, TokenKind::Str) {
                    ParserError::NoToken(_) => "No sumary provided".into(),
                    p => return Err(p),
                },
            };
            let description = match lexer.next() {
                Some(token) => match token {
                    Token {
                        kind: TokenKind::Str,
                        text,
                        ..
                    } => Some(text),
                    token => match parser_error(token, TokenKind::Str) {
                        ParserError::NoToken(_) => None,
                        p => return Err(p),
                    },
                },
                None => None,
            };
            Ok(Reminder::new(
                crate::When::Day(day, hour, minute),
                summary,
                description,
            ))
        }
        Err(token) => Err(parser_error(token, TokenKind::Str)),
    }
}

pub fn parse_date<C: Iterator<Item = char>>(lexer: &mut Lexer<C>) -> Result<Reminder, ParserError> {
    match lexer.expect_token(TokenKind::Num) {
        Ok(token) => {
            let day = token.text.parse::<u32>().unwrap();
            let month = match lexer.expect_token(TokenKind::Num) {
                Ok(token) => token.text.parse::<u32>().unwrap(),
                Err(token) => return Err(parser_error(token, TokenKind::Num)),
            };
            let year = match lexer.expect_token(TokenKind::Num) {
                Ok(token) => token.text.parse::<i32>().unwrap(),
                Err(token) => return Err(parser_error(token, TokenKind::Num)),
            };
            let hour = match lexer.expect_token(TokenKind::Num) {
                Ok(token) => token.text.parse::<u32>().unwrap(),
                Err(token) => return Err(parser_error(token, TokenKind::Num)),
            };
            let minute = match lexer.expect_token(TokenKind::Num) {
                Ok(token) => token.text.parse::<u32>().unwrap(),
                Err(token) => return Err(parser_error(token, TokenKind::Num)),
            };
            let summary = match lexer.expect_token(TokenKind::Str) {
                Ok(token) => token.text,
                Err(token) => match parser_error(token, TokenKind::Str) {
                    ParserError::NoToken(_) => "No sumary provided".into(),
                    p => return Err(p),
                },
            };
            let description = match lexer.next() {
                Some(token) => match token {
                    Token {
                        kind: TokenKind::Str,
                        text,
                        ..
                    } => Some(text),
                    token => match parser_error(token, TokenKind::Str) {
                        ParserError::NoToken(_) => None,
                        p => return Err(p),
                    },
                },
                None => None,
            };
            Ok(Reminder::new(
                crate::When::Date(day, month, year, hour, minute),
                summary,
                description,
            ))
        }
        Err(token) => Err(parser_error(token, TokenKind::Num)),
    }
}

fn parser_error(token: Token, expected: TokenKind) -> ParserError {
    match token {
        Token {
            kind: TokenKind::Invalid,
            text,
            loc,
        } => ParserError::InvalidToken(loc, text),
        Token {
            kind: TokenKind::UnclosedStr,
            text,
            loc,
        } => ParserError::UnclosedStr(loc, text),
        Token {
            kind: TokenKind::End,
            loc,
            ..
        } => ParserError::NoToken(loc),
        Token { kind, text, loc } => ParserError::UnexpectedToken(loc, kind, text, expected),
    }
}

pub enum ParserError {
    NoToken(Loc),
    UnexpectedToken(Loc, TokenKind, String, TokenKind), //token kind found, text of token, token kind expected
    UnclosedStr(Loc, String),
    InvalidDay(Loc, String),
    InvalidNum(Loc, i32, i32, i32), //num found, min, max
    InvalidToken(Loc, String),
}

pub enum Command {
    Quit,
    Remind,
    List,
    Edit,
    Help,
    Invalid(Option<Token>),
}
